# hi

1. Install Node.js (4.0.0 or later)

1. Install dependencies

    ```
    $ npm install
    ```

1. Serve the presentation and monitor source files for changes

    ```
    $ npm start
    ```

1. Open `http://localhost:8000` to view your presentation

You can change the port by using `npm start -- --port=8001` .

