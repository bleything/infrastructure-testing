## Lab 3

kitchen-terraform

let's use it with a GKE project

---

your new terraform repository

```bash
$ cd ~/infrastructure-testing/lab\ 3/
$ tree
.
├── main.tf
└── test
    └── integration
        └── verify
            ├── controls
            │   └── k8s.rb
            └── inspec.yml
```

---

## thank you !

source https://goo.gl/iNFj17

feedback https://goo.gl/XFYCWt


<img src="./img/cloud_icon_color.png" width="20%" border=0>
<img src="./img/terraform.png" width="20%" border=0>
<img src="./img/inspec_logo.png" width="30%" border=0>
